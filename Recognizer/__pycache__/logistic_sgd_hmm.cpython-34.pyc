�
	�W�I  �               @   s�   d  Z  d Z d d l Z d d l Z d d l Z d d l Z d d l Z d d l Z d d l Z d d l	 Z	 d d l
 j Z Gd d �  d e � Z d d �  Z d d	 d
 d d d � Z e d k r� e �  n  d S)a�  
This tutorial introduces logistic regression using Theano and stochastic
gradient descent.

Logistic regression is a probabilistic, linear classifier. It is parametrized
by a weight matrix :math:`W` and a bias vector :math:`b`. Classification is
done by projecting data points onto a set of hyperplanes, the distance to
which is used to determine a class membership probability.

Mathematically, this can be written as:

.. math::
  P(Y=i|x, W,b) &= softmax_i(W x + b) \
                &= rac {e^{W_i x + b_i}} {\sum_j e^{W_j x + b_j}}


The output of the model or prediction is then done by taking the argmax of
the vector whose i'th element is P(Y=i|x).

.. math::

  y_{pred} = argmax_i P(Y=i|x,W,b)


This tutorial presents a stochastic gradient descent optimization method
suitable for large datasets.


References:

    - textbooks: "Pattern Recognition and Machine Learning" -
                 Christopher M. Bishop, section 4.3.2

zrestructedtext en�    Nc               @   sR   e  Z d  Z d Z d d �  Z d d �  Z d d �  Z d d	 �  Z d
 d �  Z d S)�LogisticRegressiona5  Multi-class Logistic Regression Class

    The logistic regression is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`. Classification is done by projecting data
    points onto a set of hyperplanes, the distance to which is used to
    determine a class membership probability.
    c             C   s�   t  j d t j | | f d t  j j �d d d d � |  _ t  j d t j | f d t  j j �d d d d � |  _ t j	 j
 t j | |  j � |  j � |  _ t j |  j d d	 �|  _ g  |  _ |  j |  j g |  _ d
 S)a   Initialize the parameters of the logistic regression

        :type input: theano.tensor.TensorType
        :param input: symbolic variable that describes the input of the
                      architecture (one minibatch)

        :type n_in: int
        :param n_in: number of input units, the dimension of the space in
                     which the datapoints lie

        :type n_out: int
        :param n_out: number of output units, the dimension of the space in
                      which the labels lie

        �value�dtype�name�W�borrowT�b�axis�   N)�theano�shared�numpy�zeros�config�floatXr   r   �T�nnet�softmax�dot�p_y_given_x�argmax�y_predZ	salvation�params)�self�input�n_in�n_out� r   �9/home/elaynne/TCC/tcc_game/Recognizer/logistic_sgd_hmm.py�__init__:   s     			+	zLogisticRegression.__init__c             C   s&   t  j j t  j | |  j � |  j � S)z�
        Given an input (a vector of pixels), create a function that maps the
        input to a logistic probability function given the weight matrix and bias vector.

        HINT: look for the softmax function
        )r   r   r   r   r   r   )r   �imager   r   r   �class_probability_funcp   s    z)LogisticRegression.class_probability_funcc             C   s4   t  j t  j |  j � t  j | j d � | f � S)a�  Return the mean of the negative log-likelihood of the prediction
        of this model under a given target distribution.

        .. math::

            rac{1}{|\mathcal{D}|} \mathcal{L} (	heta=\{W,b\}, \mathcal{D}) =
            rac{1}{|\mathcal{D}|} \sum_{i=0}^{|\mathcal{D}|}
                \log(P(Y=y^{(i)}|x^{(i)}, W,b)) \
            \ell (	heta=\{W,b\}, \mathcal{D})

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label

        Note: we use the mean instead of the sum so that
              the learning rate is less dependent on the batch size
        r   )r   �mean�logr   �arange�shape)r   �yr   r   r   �negative_log_likelihoody   s    z*LogisticRegression.negative_log_likelihoodc             C   sw   | j  |  j j  k r< t d d | j d |  j j f � � n  | j j d � rj t j t j |  j | � � St	 �  � d S)aQ  Return a float representing the number of errors in the minibatch
        over the total number of examples of the minibatch ; zero one
        loss over the size of the minibatch

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label
        z+y should have the same shape as self.y_predr&   r   �intN)
�ndimr   �	TypeError�typer   �
startswithr   r"   �neq�NotImplementedError)r   r&   r   r   r   �errors�   s    !zLogisticRegression.errorsc             C   sC   t  j |  j j k r< t d d t  j d |  j j f � � n  |  j S)aQ  Return a float representing the number of errors in the minibatch
        over the total number of examples of the minibatch ; zero one
        loss over the size of the minibatch

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label
        z+y should have the same shape as self.y_predr&   r   )r&   r)   r   r*   r+   )r   r   r   r   �output�   s
    !zLogisticRegression.outputN)	�__name__�
__module__�__qualname__�__doc__r   r!   r'   r/   r0   r   r   r   r   r   1   s   6	 r   c              C   sA  t  j j |  � \ } } | d k r� t  j j |  � r� t  j j t  j j t � d d d |  � } t  j j | � s� | d k r� | }  q� n  t  j j |  � r� | d k r� d d l } d d l } d d l } d } t	 d | � | j
 j | |  � n  t	 d	 � t j |  d
 � } t j | d d �\ } } }	 t j d � }
 t d d
 � � } t j | � } Wd QXt d d
 � � } t j | � } Wd QXt j g  | d d d � � } t j d d � } t j j | � | } | d }
 |
 d d � d d � f } | | } |
 | d d � f }
 d } |
 d d � d d � f } | d d � } | d d d � d d � f } t j g  | d d d � � d d � } | } | } | j �  d d d � } | | | � \ } } | | | � \ } } | | | � \ } } | | g  � \ } } | | f | | f | | f | | f g } | S)zk Loads the dataset

    :type dataset: string
    :param dataset: the path to the dataset (here MNIST)
    � r   z..�datazmnist.pkl.gzNz>http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gzzDownloading data from %sz... loading data�rb�encoding�latin1znumeros.txtzbase_fbank_d.pklzbase_fbank_teste_d.pklr
   i�G  �
   ip  i|G  i�,  Tc             S   sj   t  j t j |  d t  j j �d | �} t  j t j | d t  j j �d | �} | t j | d � f S)a�   Function that loads the dataset into shared variables

        The reason we store our dataset in shared variables is to allow
        Theano to copy it into the GPU memory (when code is run on GPU).
        Since copying data into the GPU is slow, copying a minibatch everytime
        is needed (the default behaviour if the data is not in a shared
        variable) would lead to a large decrease in performance.
        r   r   �int32)r   r   r   �asarrayr   r   r   �cast)Zdata_xZdata_yr   Zshared_xZshared_yr   r   r   �shared_dataset  s    		z!load_data.<locals>.shared_dataset)�os�path�split�isfile�join�__file__�urllib.request�urllib.parse�urllib.error�print�request�urlretrieve�gzip�open�pickle�loadr   �loadtxt�dill�appendr$   �random�shuffle�close) �datasetZdata_dirZ	data_file�new_path�urllib�origin�fZ	train_setZ	valid_setZtest_setZdados�baseZ
base_teste�labels�listaZtestemax�iZ
train_setxZ
train_setyZ
valid_setxZ
valid_setyZ	test_setxZ	test_setyr>   �
test_set_x�
test_set_y�valid_set_x�valid_set_y�train_set_x�train_set_yZyaZrvalr   r   r   �	load_data�   s\    		$
 

 *
rd   g�p=
ף�?i�  zmnist.pkl.gziX  c       '         s�  t  | � } | d \ } } | d \ } } | d \ }	 }
 | j d d � j d | } | j d d � j d | } |	 j d d � j d | } t d � t j �  } t j d � } t j d � } t d	 | d
 d& d d � } | j	 | � } t
 j d | g d | j | � d i |	 | | | d | � | 6|
 | | | d | � | 6� �  t
 j d | g d | j | � d i | | | | d | � | 6| | | | d | � | 6� � t j d | d | j � } t j d | d | j � } | j | j |  | f | j | j |  | f g } t
 j d | g d | d | d i | | | | d | � | 6| | | | d | � | 6� } t d � d } d } d } t | | d � } t j } d } t j �  } d } d } xz| | k  rV| rV| d } xVt t | � � D]B}  | |  � }! | d | |  }" |" d | d k r9� f d d �  t t | � � D� }# t j |# � }$ t |$ � t d | |  d | |$ d f � |$ | k  r9|$ | | k  r�t | |" | � } n  |$ } �  f d d �  t t | � � D� }% t j |% � } t d | |  d | | d f � q9n  | |" k rd } PqqWq�Wt j �  }& t d | d | d f � t d  | d! | |& | f � t d" t j j t � d d# |& | d$ t j �d% S)'a  
    Demonstrate stochastic gradient descent optimization of a log-linear
    model

    This is demonstrated on MNIST.

    :type learning_rate: float
    :param learning_rate: learning rate used (factor for the stochastic
                          gradient)

    :type n_epochs: int
    :param n_epochs: maximal number of epochs to run the optimizer

    :type dataset: string
    :param dataset: the path of the MNIST dataset file from
                 http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz

    r   r
   �   r   Tz... building the model�xr&   r   r   �   r   r:   �inputs�outputsZgivens�costZwrt�updatesz... training the modeli'  gףp=
��?g        Fc                s   g  |  ] } �  | � � q Sr   r   )�.0r]   )�validate_modelr   r   �
<listcomp>�  s   	z*sgd_optimization_mnist.<locals>.<listcomp>z1epoch %i, minibatch %i/%i, validation error %f %%g      Y@c                s   g  |  ] } �  | � � q Sr   r   )rl   r]   )�
test_modelr   r   rn   �  s   	z>     epoch %i, minibatch %i/%i, test error of best model %f %%zUOptimization complete with best validation score of %f %%,with test performance %f %%z.The code run for %d epochs, with %f epochs/secg      �?zThe code for file z ran for %.1fs�fileNi  )rd   �	get_valuer%   rH   r   Zlscalar�matrixZivectorr   r'   r   �functionr/   Zgradr   r   �minr   �inf�time�clock�ranger(   r"   �maxr?   r@   rA   rD   �sys�stderr)'Zlearning_rateZn_epochsrU   �
batch_sizeZdatasetsrb   rc   r`   ra   r^   r_   Zn_train_batchesZn_valid_batchesZn_test_batches�indexrf   r&   Z
classifierrj   Zg_WZg_brk   Ztrain_modelZpatienceZpatience_increaseZimprovement_thresholdZvalidation_frequencyZbest_validation_lossZ
test_scoreZ
start_timeZdone_loopingZepochZminibatch_indexZminibatch_avg_cost�iterZvalidation_lossesZthis_validation_lossZtest_lossesZend_timer   )ro   rm   r   �sgd_optimization_mnist;  s�    
		#		#		#
	


r   �__main__)r4   �__docformat__rM   rK   r?   rz   rv   rP   r   r   Ztheano.tensor�tensorr   �objectr   rd   r   r1   r   r   r   r   �<module>"   s"   �u�